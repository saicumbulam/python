import numpy as np

#declare dummy variables
lon = np.arange(45,101,2)
lat = np.arange(-30,25,2.5)
z = np.arange(0,200,10)
x = np.random.randint(10,25, size=(len(lon), len(lat), len(z)))
noise = np.random.rand(len(lon),len(lat),len(z))
temp_data = x+noise

#create the dataset
import netCDF4 as nc4
f = nc4.Dataset('sample.nc','w',format='NETCDF4')

#create groups for arranging data
tempgrp = f.createGroup('Temp_data')

#specifying dimension
tempgrp.createDimension('lon',len(lon))
tempgrp.createDimension('lat',len(lat))
tempgrp.createDimension('z',len(z))
tempgrp.createDimension('time',None)

#Building variables
longitude = tempgrp.createVariable('Longitude','f4','lon')
latitude = tempgrp.createVariable('Latitude','f4','lat')
levels = tempgrp.createVariable('Levels','f4','z')
temp = tempgrp.createVariable('Temperature','f4',('time','lon','lat','z'))
time = tempgrp.createVariable('Time','f4','time')

print(f)
print(f.groups['Temp_data'])

longitude[:] = lon #The "[:]" at the end of the variable instance is necessary
latitude[:] = lat
levels[:] = z
temp[0,:,:,:] = temp_data

#get time in days since Jan 01,01
from datetime import datetime
today = datetime.today()
time_num = today.toordinal()
time[0] = time_num

#Add global attributes
f.description = "Example dataset containing one group"
f.history = "Created " + today.strftime("%d/%m/%y")

#Add local attributes to variable instances
longitude.units = 'degrees east'
latitude.units = 'degrees north'
time.units = 'days since Jan 01, 0001'
temp.units = 'Kelvin'
levels.units = 'meters'
temp.warning = 'This data is not real!'

f.close()