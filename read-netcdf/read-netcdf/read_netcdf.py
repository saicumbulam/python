f = nc4.Dataset('sample.nc','r')
tempgrp = f.groups['Temp_data']
print("meta data for the dataset:")
print(f)
print("meta data for the Temp_data group:")
print(tempgrp)
print("meta data for Temperature variable:")
print(tempgrp.variables['Temperature'])

#query for list of variables
print(tempgrp.variables.keys())

#get units from the variables
time_vble = tempgrp.variables['Time']
print(time_vble.ncattrs())
print(time_vble.getncattr('units'))

#variable has many attributes
temp_vble = tempgrp.variables['Temperature']
for attr in temp_vble.ncattrs():
    print(attr + ': ' + temp_vble.getncattr(attr))


#Accesing data from a variable
zlvls = tempgrp.variables['Levels'][:]

#access subset of data. Wanted data from first level
temp_0z = tempgrp.variables['Temperature'][0,:,:,0]
