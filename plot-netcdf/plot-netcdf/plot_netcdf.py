from netCDF4 import Dataset as NetCDFFile 
import csv
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.basemap import Basemap
from pylab import rcParams

nc = NetCDFFile('C:/Users/SThangaraj/Documents/OR_GLM-L2-LCFA_G16_s20173550000000_e20173550000200_c20173550000227.nc') # note this file is 2.5 degree, so low resolution data
print(nc.dimensions.keys())
#for x in nc.variables.keys():
#	print("keys:{}, values:{} ".format(x,nc.variables[x]))

#lat = nc.variables['event_lat'][:]
#print(type(nc.variables['event_lat']))
#print(len(lat))
#header1 = [str(x) for x in nc.variables.keys()]
#row1 = [nc.variables[x][:].tolist() for x in header1]
##row1 = [nc.variables[x][:].tolist() if isinstance(x, ArrayType) else [nc.variables[x][:]] for x in header1]
##row2 = [tuple(l) for l in row1]
##row3 = zip(*row2) 
##print(row3)
##csv writer
##csv.register_dialect('myDialect', delimiter=',', quoting=csv.QUOTE_NONE)
#myFile = open('C:/Users/SThangaraj/Documents/example.csv','w')
#with myFile:
#	writer = csv.writer(myFile)
#	#writer.writerow(header1)
#	writer.writerows(row1)
#print("csv has been written")

#lat = nc.variables['flash_lat'][:].tolist()
#lon = nc.variables['flash_lon'][:].tolist()


#rcParams['figure.figsize'] = (15,15)

#from mpl_toolkits.basemap import Basemap
#import matplotlib.pyplot as plt
#import numpy as np
 
#map = Basemap(projection='merc',
#    resolution = 'h', area_thresh = 0.1,
#    llcrnrlon=-136.05, llcrnrlat=-52.48,
#    urcrnrlon=196.87, urcrnrlat=72.07)
 
#map.drawcoastlines()
#map.drawcountries()
#map.fillcontinents(color = 'coral')
#map.drawmapboundary()

#x,y = map(lon, lat)
#map.plot(x, y, 'bo', markersize=0.2)
#plt.show()
#print("Map created")

#import csv

#arr = [['red', '361', '0'],
#      ['blue', '1', '0'],
#      ['orange', '77', '0'],
#      ['cream', '660', '73'],
#      ['ivory', '159', '0']]

#with open('C:/Users/SThangaraj/Documents/output.csv','w') as f:
#    writer = csv.writer(f)
#    writer.writerow(['color', 'total', 'fail'])
#    writer.writerows(arr)

